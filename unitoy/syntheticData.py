'''
This submodule builds synthetic data for demand and units
'''
import numpy as np
import pandas as pd

### basic default parameters
__bParams__={'D': 10, #demand first period value 
        'T': 48, # time horizon
        'm1': 0.3, 
        'm2': 0.5, 
        'm': 100,
        'C':1}


def make_data1_from_demand(demand=None, param=None):
    '''
    If demand is None, a syntetic generator is called
    demand: pandas Series (default None)
        if None, synthetic demand is used

    param: dict, optional fields:
        'D': str demand for the first period
        'T': int, time size horizon
        'm1':
        'm2':
        'C':
    '''
    print('Generating syntetic data from demand')
    if demand is None:
        print('Demand not supplied. Syntetic demand will be generated for this use')
        D = demand_model1(param=param)
    else:
        D = demand

    aux = np.zeros(4)
    for i, key in enumerate(['m', 'm1','m2','C']):
        if  (isinstance(param, dict) and (key in param)):
            aux[i] = param[key]
        else:
            aux[i] =  __bParams__[key]
    m, m1, m2, C = aux
    m = int(m)

    Qbase, Qav  = np.quantile(D, np.cumsum([0.25, 0.625]))
    Db = D[D<=Qbase].mean()
    Da = D[(D>Qbase) & (D<=Qav)].mean()
    Dp = D[D>Qav].mean()

    data = pd.DataFrame(np.zeros((m , 7)), columns=['C', 'F','pmin', 'pmax', 'ramp','state0', 'p0'], 
            index= ['th-{}'.format(i) for i in range(1,m + 1)])

    # print('m1 ={}\n m2={}\n m={}'.format(m1, m2, m))
    m1 = int(m1 * m)
    m2 = int(m2 * m)
    # print('m1 ={}\n m2={}'.format(m1, m2))
    
    ## setting unit cost
    data['C'].iloc[:m1] = C
    data['C'].iloc[m1: m1 + m2] = 2 *C
    data['C'].iloc[m1 + m2:] = 4 *C

    ## setting capacity
    data['pmax'].iloc[:m1] = Db/m1
    data['pmax'].iloc[m1: m1 + m2] = Da/(m1+m2)
    data['pmax'].iloc[m1 + m2:] = Dp/m

    ## setting minim cap

    ## setting ramp
    data['ramp'].iloc[:m1] = None
    data['ramp'].iloc[m1: m1 + m2] = 0.05 * (data['pmax'].iloc[m1: m1 + m2] - data['pmin'].iloc[m1: m1 + m2])
    data['ramp'].iloc[m1 + m2:] = 0.5 * (data['pmax'].iloc[m1 + m2:] - data['pmin'].iloc[m1 + m2:])

    ## set staring price
    data['F'] = 0.1 * data['C']
    
    ## setting initial state
    data['state0'] = np.random.randint(0, 2, size=m)


    ## setting initial generation
    data['p0'] = np.random.rand(m)

    data['p0'] = (data['pmin'] + data['p0'] * (data['pmax'] - data['pmin'])) * data['state0']

    return data



def demand_model1(param=None):
    '''
    para:dict {'T':None, 'D':None},
        T:int number of periods
        D0: float, inital seed for demand
    if any of them is None, a default value will be used.
    '''
    if param is not None:
        if 'T' in param:
            T = param['T']
        else:
            T = __bParams__['T']

        if 'D' in param:
            D0 = param['D']
        else:
            D0 = __bParams__['D']
    else:
        T = __bParams__['T']
        D0 = __bParams__['D']

    if T == 1:
        D = pd.Series([D0])
    else:
        D =  D0*(1  + 0.5 * np.random.randn(T))
        D[D<= 0.1 * D0] = D0
        D = pd.Series(D)
    return D
