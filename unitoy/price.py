import numpy as np
import pandas as pd
import gurobipy as grb
from unitoy import toys as ut

def linearRelaxation(toy):
    price = None
    optVal = None
    primalResult = None
    extraResult = None

    if toy.primalDict is None:
        print('Primal model not yet built...')
    else:
        linRelaxDict = ut.copyModelDict(toy.primalDict, relax=True)
    dual = []
    optVal, primalResult, extraResult = ut.solveModelDict(linRelaxDict, dual=dual)

    if dual:
        price = dual[0]['demand']
    else:
        price = None

    return price, optVal, primalResult, extraResult



def IP(toy):
    price = None
    optVal = None
    primalResult = None
    extraResult = None
    if toy.primalDict is None:
        print('Primal model not yet built...')
    else:
        rOptVAl, rPrimalResult , extraResult= toy.solveModel()
        if rOptVAl is not None:
            linRelaxDict = ut.copyModelDict(toy.primalDict, relax=True)
            u = linRelaxDict['var']['commit(u)']
            
            uCon= linRelaxDict['model'].addConstrs((u[i] == rPrimalResult.loc[i,'commit(u)'] for i in u), name='commit_fixed')
            linRelaxDict['extraCon']['commit_fixed'] = uCon
            dual = []
            optVal, primalResult, extraResult = ut.solveModelDict(linRelaxDict, dual=dual)
            if dual:
                price = dual[0]
            else:
                price = None
        else:
            print('primal model failed to be solved')

    return price, optVal, primalResult, extraResult

def minUplift(toy, maxIter=100,globalLog=None, xLog=None, nsoMethod='python'):
    import pynso.proxBundle as pb
    price = None
    optVal = None
    primalResult = None
    extraResult = None

    if toy.primalDict is None:
        print('Primal model not yet built...')
        price = None
        result = None
    else: 
        price, optVal, primalResult, extraResult = linearRelaxation(toy)

        if optVal is not None:
            def bb(lmda, mode=0, context=None): 
                val, superGrad, status = toy.LR_BB(lmda)
                if mode == 0:
                    return -val, status
                elif mode == 1:
                    return -superGrad, status
                elif mode == 2:
                    return -val, -superGrad, status
            def fg(lmda):
                val, superGrad, status = toy.LR_BB(lmda) 
                return -val, -superGrad

            result = pb.minimize(price.values, fg=fg, maxIter=maxIter, globalLog=globalLog, xLog=xLog, method=nsoMethod)
            if result.status !=0:
                print('nonsmooth solver failed to find dual solution. status={}'.format(result.status))
                price = result.x_best
        else:
            print('relaxed model failed to be solved')
            price = None
            result = None

    return price, result


# def econ(toy, priceTarget=None, beta=0.1, dispatchPlan=None):
    # price = None
    # optVal = None
    # if toy.primalDict is None:
        # print('Primal model not yet built...')
    # else:
        # if dispatchPlan is None:
            # print('No dispatch plan provided. It will be computed from toy')
            # rOptVAl, rPrimalResult , extraResult= toy.solveModel()
            # if rOptVAl is not None:
                
                # uCon= linRelaxDict['model'].addConstrs((u[i] == rPrimalResult.loc[i,'commit(u)'] for i in u), name='commit_fixed')
                # linRelaxDict['extraCon']['commit_fixed'] = uCon
                # dual = []
                # optVal, primalResult, extraResult = ut.solveModelDict(linRelaxDict, dual=dual)
                # if dual:
                    # price = dual[0]
                # else:
                    # price = None
            # else:
                # print('primal model failed to be solved')

    # return price, optVal
