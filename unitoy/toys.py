###
import os
import numpy as np
import pandas as pd
import gurobipy as grb
import unitoy.models 
from unitoy import syntheticData as sd
###

__dataDir__ = os.path.join(os.path.dirname(__file__), 'data')
__demandDir__ =  os.path.join(__dataDir__, 'demand')
__unitSetDir__ =  os.path.join(__dataDir__, 'unitSet')
__FCFDir__ =  os.path.join(__dataDir__, 'FCF')

__availableDataSetDict__ = {'case1':'d2unit_convex.csv', 'case2':'d2unit_discontinuous.csv', 'case3':'d2unit_nonconvex.csv',}
__availableDemandDict__ = {'48T':'demand48_1.csv'}

availableModels={'model1': unitoy.models.model1}
defaulModel = 'model1'
defaultDemandFun=sd.demand_model1

availableDataSets = __availableDataSetDict__
availableDemand = __availableDemandDict__
defaultFCF = 'FCFMarcelo.csv'
#availableDemand = list(__availableDemandDict__.keys())
#availableDataSets = list(__availableDemandDict__.keys())


def getDemand(code='48T'):
    if code not in availableDemand:
        print('Demand code {} is not available'.format(code))
        demand = None
    else:
        demand = pd.read_csv(os.path.join(__demandDir__, __availableDemandDict__[code]), index_col=0).iloc[:,0]
    return demand

def copyModelDict(mDict, relax=False):
    mDict['model'].update()
    if relax:
        model =mDict['model'].relax()
    else:
        model = mDict['model'].copy()

    newVar = {}
    for key in mDict['var']:
        if  isinstance(mDict['var'][key], dict):
            newVar[key] = grb.tupledict({index: model.getVarByName(mDict['var'][key][index].varName) for index in  mDict['var'][key]})
        else:
            newVar[key] =  model.getVarByName(mDict['var'][key].varName)

    # p = grb.tupledict({index: model.getVarByName(mDict['p'][index].varName) for index in  mDict['p']})
    # u = grb.tupledict({index: model.getVarByName(mDict['u'][index].varName) for index in  mDict['u']})
    # z = grb.tupledict({index: model.getVarByName(mDict['z'][index].varName) for index in  mDict['z']})
    if mDict['extraVar'] is not None:
        extraVar ={}
        for key in mDict['extraVar']:
            if  isinstance(mDict['extraVar'][key], dict):
                extraVar[key] = grb.tupledict({index: model.getVarByName(mDict['extraVar'][key][index].varName) for index in  mDict['extraVar'][key]})
            else:
                extraVar[key] =  model.getVarByName(mDict['extraVar'][key].varName)
    else:
        extraVar = None

    if mDict['extraCon'] is not None:
        extraCon ={}
        for key in mDict['extraCon']:
            if  isinstance(mDict['extraCon'][key], dict):
                extraCon[key] = {index: model.getConstrByName(mDict['extraCon'][key][index].constrName) for index in  mDict['extraCon'][key]}
            else:
                extraCon[key] =  model.getConstrByName(mDict['extraCon'][key].constrName)
    else:
        extraCon = None
    
    modelDict=dict(model=model, var=newVar, extraVar=extraVar, extraCon=extraCon, extraInfo=None)
    return modelDict



def solveModelDict(modelDict, dual=None, MIPGap=None): 
    __statusDict__= { grb.GRB.LOADED:'LOADED', grb.GRB.OPTIMAL:'OPTIMAL', grb.GRB.INFEASIBLE:'INFEASIBLE', grb.GRB.INF_OR_UNBD:'INF_OR_UNBD', grb.GRB.UNBOUNDED:'UNBOUNDED', grb.GRB.CUTOFF:'CUTOFF', grb.GRB.ITERATION_LIMIT:'ITERATION_LIMIT', grb.GRB.NODE_LIMIT:'NODE_LIMIT', grb.GRB.TIME_LIMIT:'TIME_LIMIT', grb.GRB.SOLUTION_LIMIT:'SOLUTION_LIMIT', grb.GRB.INTERRUPTED:'INTERRUPTED', grb.GRB.NUMERIC:'NUMERIC', grb.GRB.SUBOPTIMAL:'SUBOPTIMAL', grb.GRB.INPROGRESS:'INPROGRESS', grb.GRB.USER_OBJ_LIMIT:'USER_OBJ_LIMIT'} 


    # p = modelDict['p']
    # u = modelDict['u']
    # z = modelDict['z']
    model = modelDict['model']

    statusDict = __statusDict__
    result = {}
    extraResult = {}
    if MIPGap is not None: 
        model.Params.MIPGap=MIPGap #JP: defing gap size

    model.optimize()
    if model.status == grb.GRB.OPTIMAL: 
        optVal = model.objVal
        for vvKey, vv in modelDict['var'].items():
            if  isinstance(vv, dict):
                result[vvKey] = {pName: pVar.x for pName, pVar in vv.items()}
            else:
                result[vvKey] = {vv.varName: vv.x}
        
        if modelDict['extraVar'] is not None:
            for vvKey, vv in modelDict['extraVar'].items():
                if  isinstance(vv, dict):
                    extraResult[vvKey] = pd.Series({pName: pVar.x for pName, pVar in vv.items()})
                else:
                    extraResult[vvKey] = pd.Series({vv.varName: vv.x}, index=[vv.varName])

        result = pd.DataFrame(result)
        result.index.set_names(['unit', 'time'], inplace=True)

        if dual is not None:
            res = {}
            for cc in modelDict['extraCon']:
                con = modelDict['extraCon'][cc]
                res[cc] = pd.Series({ i: con[i].Pi for i in con})
            dual.append(res)

    else:
        print(statusDict[model.status])
        optVal = None
        result = None
    return optVal, result, extraResult



class toy():
    data=None 
    FCFTable=None
    primalDict=None

    #### model related variables
    __demand__=None ##demand used for building the model
    # Dual information
    LRDict=None

    def __init__(self, primalDict=None, dataSet=None, csvFile=None, table=None, demand=None, m=100, FCFTable=None):
        ###
        if primalDict is None:
            print('Study case will be built from data set')
            dataRead = False
            if dataSet is not None:
                if dataSet in availableDataSets:
                    print('Data set from library')
                    fileName = os.path.join(__unitSetDir__, availableDataSets[dataSet])
                    self.data=pd.read_csv(fileName, index_col=0)
                    dataRead = True
                else:
                    print('Dataset {} is not available'.format(dataSet))
                    dataRead = False
            if dataRead is False:
                if table is not None:
                    print('DataSet from pandas DataFrame provided...')
                    self.data=table
                elif csvFile is not None:
                    print('DataSet from  csv file provided...')
                    self.data = pd.read_csv(csvFile, index_col=0)
                else: 
                    print('Sythetic data will be generated from demand')
                    
                    D = self.__read_demand__(demand=demand)
                    if D is None:
                        print('No demand provided... Synthectic demand will be built only for building the units data set')
                        D = sd.demand_model1()
                    self.data = sd.make_data1_from_demand(demand=D, param=None)
                    self.__demand__=D
        else:
            self.primalDict = primalDict

        # reading FCF
        if FCFTable is not None:
            self.FCFTable = self.__read_FCF__(FCFTable)

    def __read_demand__(self, demand=None):
        '''
        read de demand from the argument
        demand:str will read from a csv file
        demand:pandas Series
        demand:list or numpy array, float,  the method will build a pandas series
        if None, it will return a None value
        '''
        if demand is not None: 
            if isinstance(demand, str):
                print('loading demand from file {}'.format(demand))
                D = pd.read_csv(demand)
                if len(D.columns) > 1:
                    D.set_index(D.columns[0], inplace=True)
                D = D.iloc[:,0]
            elif isinstance(demand, (list, np.ndarray)): 
                print('loading demand from vector ')
                D = pd.Series(demand)
            elif isinstance(demand, pd.core.series.Series):
                D = demand
            elif isinstance(demand, pd.DataFrame):
                D = demand.iloc[:,0]
                print('Warning: Demand provided as DataFrame. First column taken')
            elif isinstance(demand, (float, int)): 
                D = pd.Series([demand])
            else:
                print('Reading demand failed')
                D = None
        else:
            D=None
        return D

    def __read_FCF__(self, FCFTable=None):
        '''
        read de FCFTable from the argument
        FCFTable:str will read from a csv file
        FCFTable:pandas DataFrame. First colums index (cuts), it should have a column 'constant'
        if None, it will return a None value
        '''
        if FCFTable is not None: 
            if isinstance(FCFTable, str):
                if FCFTable.lower() == 'default':
                    print('Loading {} FCF'.format(FCFTable))
                    cuts = pd.read_csv(os.path.join(__FCFDir__ , defaultFCF ), index_col=0)
                else: 
                    print('loading FCFTable from file {}'.format(FCFTable))
                    cuts = pd.read_csv(FCFTable, index_col=0)
            elif isinstance(FCFTable, pd.DataFrame):
                cuts = FCFTable
            else:
                print('Reading FCFTable failed')
                cuts = None
        else:
            cuts=None

        #checking if FCF has a proper structure
        if cuts is not None:
            if len(cuts.columns) < 2:
                print('reading FCF: FCFTable does not have enough columns')
                cuts = None
            elif 'constant' not in cuts.columns:
                print('reading FCF: FCFTable does not have columns "constant"')
                cuts = None
        return cuts

    def buildModel(self, model=None, demand=None, unitList=None, param=None, FCFTable=None):
        if model is None:
            model = defaulModel
            modelFun = availableModels[model]
            print('Default model will be used {}'.format(model))
        elif isinstance(model, str):
            if model in availableModels:
                modelFun = availableModels[model]
            else:
                modelFun = availableModels[defaulModel]
                print('Model {} not available. Default model will be used {}'.format(model, defaulModel))
        else:
            print('Model provided by user will be used. ')
            modelFun = model

        if unitList is None:
            unitList = self.data.index
        
        demand = self.__read_demand__(demand=demand)

        if demand is None:
            print('Demand not provided. Syntetic demand will be used')
            demand = defaultDemandFun()

        if FCFTable is None:
            auxFCFTable= self.FCFTable
        else:
            auxFCFTable = self.__read_FCF__(FCFTable)

        modelDict =  modelFun(self, demand=demand, unitList=unitList, param=param, FCFTable=auxFCFTable)
        modelDict['timeIndex']= list(demand.index)
        modelDict['unitList']=unitList

        self.primalDict = modelDict

        self.__demand__ = demand

    def FCF(self, vol):
        if self.FCFTable is not None: 
            FCFTable = self.FCFTable
            aux = pd.Series((vol/3) * np.ones(len(FCFTable.columns)), index=FCFTable.columns)
            aux['constant'] =1
            res = (FCFTable * aux).sum(axis=1).max()
        else:
            res=None
        return res

    def solveModel(self, modelDict='primal', dual=None, MIPGap=None):
        if modelDict == 'primal':
            mDict = self.primalDict
        else:
            mDict = self.LRDict
        optVal, result, extraResult = solveModelDict(mDict, dual=dual, MIPGap=MIPGap)

        return optVal, result, extraResult

    def LR_BB(self,lmda, solution=None, MIPGap=None):
        
        D= self.__demand__

        time = list(D.index)

        if self.LRDict is None:
            LRDict = copyModelDict(self.primalDict)
            LRDict['model'].remove(LRDict['extraCon']['demand'])
            del LRDict['extraCon']['demand']
            LRDict['model'].update()
            LRDict['extraInfo'] = LRDict['model'].getObjective()
            self.LRDict= LRDict
        else:
            LRDict = self.LRDict

        p = LRDict['var']['power(p)']
        unitList = self.primalDict['unitList']
            
        # setting demand relaxed objective function
        objF = LRDict['extraInfo'] +  grb.quicksum(lmda[i] *( D[time[i]]  - p.sum('*',time[i])) for i in range(len(time)))
        LRDict['model'].setObjective(objF, sense=grb.GRB.MINIMIZE)
        LRDict['model'].update()

        optVal, result, extraResult = self.solveModel(modelDict='LRDict', MIPGap=MIPGap)
        if optVal is None:
            status = 1
        else:
            status = 0
            supgrad = np.array([  D[time[i]]  - np.array([ p[unit, time[i]].X  for unit in  unitList ]).sum() for i in range(len(time))])
            if solution is not None: solution.append(result)
        return optVal, supgrad, status


    def __str__(self):
        if self.data is not None:
            aux =  'Data set defined as:'
            msgs = [ aux, len(aux)*'=']
            msgs.append(self.data.__str__())
        else:
            msgs = ['No data set defined']
        if self.primalDict is not None:

            primalDict = self.primalDict

            aux = 'Model built: '
            msgs.extend([ aux, len(aux)*'='])

            model = primalDict['model']
            msgs.append(model.__str__())

            for aux, key in zip([ 'Main variables:',  'Tracked variables:',  'Tracked constraints:'],['var', 'extraVar', 'extraCon']):
                auxDict = primalDict[key]
                if auxDict is not None:
                    msgs.extend([ aux, len(aux)*'='])
                    msgs.extend(['\t']  +  list(auxDict.keys()))

        else:
            msgs.append('Model not yet built')

        return '\n'.join(msgs)
    
    def __repr__(self):
        return self.__str__()

def data():
    aux = 'Available unit sets:'
    ms = [ aux, len(aux)*'=', '']
    for kk in availableDataSets:
        ms.append('\t- '+ kk)

    aux = 'Available demand sets:'
    ms.extend( [ aux, len(aux)*'=', ''])
    for kk in __availableDemandDict__ :
        ms.append('\t- '+ kk)
    ms = '\n'.join(ms)
    print(ms)
